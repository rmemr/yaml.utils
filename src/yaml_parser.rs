//
// text::yaml_parser yaml and definition parser helpers
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub enum YamlType {
    Real,
    Integer,
    String,
    Boolean,
    Array,
    Hash,
    Alias,
    Null,
    BadValue,
    NewLine,
    Comment,
}
// ----------------------------------------------------------------------------
pub struct Parser<'data> {
    name: String,
    data: &'data Yaml,
}
// ----------------------------------------------------------------------------
pub struct MapParser<'data> {
    name: &'data str,
    map: &'data Hash,
    // lut: BTreeMap<String, &'data Yaml>,
}
// ----------------------------------------------------------------------------
pub struct ListParser<'data> {
    name: &'data str,
    list: &'data Array,
}
// ----------------------------------------------------------------------------
#[macro_export]
macro_rules! yaml_errmsg {
    ($msg:expr) => {{
        format!("[definition parser] {}", $msg)
    }};
    ($name:expr, $msg:expr) => {{
        format!("[definition parser] {}: {}", $name, $msg)
    }};
    ($name:ident, $msg:expr, $found:expr) => ({
        format!("[definition parser]: {}: {} found: {}", $name, $msg, $found)
    });
}
#[macro_export]
macro_rules! yaml_errmsg_unknown_key {
    ($name:expr, $key:ident) => {
        yaml_errmsg!(format!("{}: found unsupported key: {}", $name, $key))
    };
}
#[macro_export]
macro_rules! yaml_err {
    ($msg:expr) => {{
        Err(yaml_errmsg!($msg))
    }};
    ($name:ident, $msg:expr) => {{
        Err(yaml_errmsg!($name, $msg))
    }};
    ($name:expr, $msg:expr) => {{
        Err(yaml_errmsg!($name, $msg))
    }};
    ($name:ident, $msg:expr, $found:expr) => ({
        Err(yaml_errmsg!($name, $msg, $found))
    });
}
// ----------------------------------------------------------------------------
#[macro_export]
macro_rules! yaml_err_not_found {
    ($parent:expr, $elem:expr) => {
        yaml_err!(format!("element [{}/{}] not found", $parent, $elem))
    };
}
// ----------------------------------------------------------------------------
#[macro_export]
macro_rules! yaml_err_unknown_key {
    ($name:expr, $key:ident) => {
        Err(yaml_errmsg_unknown_key!($name, $key))
    };
}
// ----------------------------------------------------------------------------
#[macro_export]
macro_rules! yaml_err_unknown_setting {
    ($name:expr, $key:ident) => {
        yaml_err!(format!("{}: found unsupported setting: {}", $name, $key))
    };
}
// ----------------------------------------------------------------------------
#[macro_export]
macro_rules! yaml_transform_kv {
    ($itemname:ident, $key:ident, $value:ident) => {{
        let key = Parser::new($key, format!("{} key", $itemname)).as_str_lower();

        let itemname = format!("{}/{}", $itemname, key);
        let value = Parser::new($value, itemname.clone());

        (itemname, key, value)
    }};
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
// use std::collections::BTreeMap;
// use std::collections::HashMap;
use std::str::FromStr;

use super::yaml_rust::yaml::{Array, Hash};
use super::yaml_rust::Yaml;
// ----------------------------------------------------------------------------
// impl<'data> From<&'data Yaml> for Parser<'data> {
//     fn from(data: &'data Yaml) -> Parser<'data> {
//         Parser::new("yaml", data)
//     }
// }
// ----------------------------------------------------------------------------
impl<'data> Parser<'data> {
    // ------------------------------------------------------------------------
    pub fn new<T: Into<String>>(name: T, data: &'data Yaml) -> Parser<'data> {
        Parser {
            name: name.into(),
            data,
        }
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        &self.name
    }
    // ------------------------------------------------------------------------
    pub fn data(&self) -> &'data Yaml {
        self.data
    }
    // ------------------------------------------------------------------------
    pub fn datatype(&self) -> YamlType {
        match *self.data {
            Yaml::Real(_) => YamlType::Real,
            Yaml::Integer(_) => YamlType::Integer,
            Yaml::String(_) => YamlType::String,
            Yaml::Boolean(_) => YamlType::Boolean,
            Yaml::Array(_, _) => YamlType::Array,
            Yaml::Hash(_) => YamlType::Hash,
            Yaml::Alias(_) => YamlType::Alias,
            Yaml::Null => YamlType::Null,
            Yaml::BadValue => YamlType::BadValue,
            Yaml::NewLine(_) => YamlType::NewLine,
            Yaml::Comment(_, _) => YamlType::Comment,
        }
    }
    // ------------------------------------------------------------------------
    pub fn datatype_name(&self) -> &str {
        yaml_type(self.data)
    }
    // ------------------------------------------------------------------------
    pub fn as_str(&self) -> Result<&str, String> {
        match *self.data {
            Yaml::String(ref s) => Ok(s),
            _ => yaml_err!(format!(
                "expected type of {} to be string. found: {}",
                self.name,
                yaml_type(self.data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_string(&self) -> Result<String, String> {
        match *self.data {
            Yaml::String(ref s) => Ok(s.to_string()),
            _ => yaml_err!(format!(
                "expected type of {} to be string. found: {}",
                self.name,
                yaml_type(self.data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_lowercase_string(&self) -> Result<String, String> {
        match *self.data {
            Yaml::String(ref s) => Ok(s.to_lowercase()),
            _ => yaml_err!(format!(
                "expected type of {} to be string. found: {}",
                self.name,
                yaml_type(self.data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_bool(&self) -> Result<bool, String> {
        match *self.data {
            Yaml::Boolean(v) => Ok(v),
            _ => yaml_err!(format!(
                "expected type of {} to be bool. found: {}",
                self.name,
                yaml_type(self.data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_integer(&self) -> Result<i32, String> {
        match *self.data {
            Yaml::Integer(v) => Ok(v as i32),
            _ => yaml_err!(format!(
                "expected type of {} to be integer. found: {}",
                self.name,
                yaml_type(self.data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_long(&self) -> Result<i64, String> {
        match *self.data {
            Yaml::Integer(v) => Ok(v),
            _ => yaml_err!(format!(
                "expected type of {} to be integer. found: {}",
                self.name,
                yaml_type(self.data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_float(&self) -> Result<f32, String> {
        match *self.data {
            Yaml::Integer(v) => Ok(v as f32),
            Yaml::Real(ref string) => match f32::from_str(string) {
                Ok(v) => Ok(v),
                Err(why) => yaml_err!(format!("could not parse float {}: {}", self.name, why)),
            },
            _ => yaml_err!(format!(
                "expected type of {} to be float. found: {}",
                self.name,
                yaml_type(self.data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn as_map(&'data self) -> Result<MapParser<'data>, String> {
        MapParser::new(&self.name, self.data)
    }
    // ------------------------------------------------------------------------
    pub fn as_list(&'data self) -> Result<ListParser<'data>, String> {
        ListParser::new(&self.name, self.data)
    }
    // ------------------------------------------------------------------------
    pub fn to_list_of_strings(&'data self) -> Result<Vec<String>, String> {
        let list = ListParser::new(&self.name, self.data)?;
        let mut result = Vec::new();

        for element in list.iter() {
            result.push(element.to_string()?);
        }

        Ok(result)
    }
    // ------------------------------------------------------------------------
    pub fn to_list_of_lowercase_strings(&'data self) -> Result<Vec<String>, String> {
        let list = ListParser::new(&self.name, self.data)?;
        let mut result = Vec::new();

        for element in list.iter() {
            result.push(element.to_lowercase_string()?);
        }

        Ok(result)
    }
    // ------------------------------------------------------------------------
    pub fn to_list_of_floats(&'data self) -> Result<Vec<f32>, String> {
        let list = ListParser::new(&self.name, self.data)?;
        let mut result = Vec::new();

        for element in list.iter() {
            result.push(element.to_float()?);
        }

        Ok(result)
    }
    // ------------------------------------------------------------------------
    pub fn to_list_of_n_floats(&'data self, count: usize) -> Result<Vec<f32>, String> {
        let list = ListParser::new(&self.name, self.data)?;
        let mut result = Vec::new();

        for element in list.iter() {
            result.push(element.to_float()?);
        }
        if result.len() == count {
            Ok(result)
        } else {
            yaml_err!(
                self.name,
                format!(
                    "expected list of {} float values. found: {}",
                    count,
                    result.len()
                )
            )
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub struct KeyValueParser<'data> {
    parentid: &'data str,
    key: &'data Yaml,
    value: &'data Yaml,
}

impl<'data> KeyValueParser<'data> {
    // ------------------------------------------------------------------------
    fn new(parentid: &'data str, key: &'data Yaml, value: &'data Yaml) -> KeyValueParser<'data> {
        KeyValueParser {
            parentid,
            key,
            value,
        }
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        self.parentid
    }
    // ------------------------------------------------------------------------
    pub fn to_key_value(&self) -> Result<(&'data str, Parser<'data>), String> {
        match *self.key {
            Yaml::String(ref key) => Ok((
                key,
                Parser::new(format!("{}/{}", self.parentid, key), self.value),
            )),
            _ => yaml_err!(format!(
                "expected type of {} to be string. found: {}",
                self.parentid,
                yaml_type(self.key)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_parser_pair(&self) -> Result<(Parser<'data>, Parser<'data>), String> {
        Ok((
            Parser::new(format!("{}{{key}}", self.parentid), self.key),
            Parser::new(format!("{}{{value}}", self.parentid), self.value),
        ))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'data> MapParser<'data> {
    // ------------------------------------------------------------------------
    #[allow(clippy::new_ret_no_self)]
    pub fn new(name: &'data str, data: &'data Yaml) -> Result<MapParser<'data>, String> {
        match *data {
            Yaml::Hash(ref data) => Ok(MapParser { name, map: data }),
            _ => yaml_err!(format!(
                "expected type of {} to be table. found: {}",
                name,
                yaml_type(data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        self.name
    }
    // ------------------------------------------------------------------------
    pub fn iter(&'data self) -> Box<dyn Iterator<Item = KeyValueParser<'data>> + 'data> {
        Box::new(
            self.map
                .iter()
                .map(move |(key, value)| KeyValueParser::new(self.name, key, value)),
        )
    }
    // ------------------------------------------------------------------------
    pub fn get_value(&self, searched_key: &str) -> Option<Result<Parser<'data>, String>> {
        self.map
            .iter()
            .map(|(key, value)| KeyValueParser::new(self.name, key, value).to_key_value())
            .find(|entry| {
                entry
                    .as_ref()
                    .map(|&(key, _)| searched_key == key.to_lowercase())
                    .unwrap_or(false)
            })
            .map(|entry| entry.map(|(_, value)| value))
    }
    // ------------------------------------------------------------------------
    pub fn as_key_value(&'data self) -> Result<(&'data str, Parser<'data>), String> {
        // special case (used rather often): a map with only one key:value pair
        match self.map.len() {
            1 => match self.map.iter().next() {
                Some(entry) => KeyValueParser::new(self.name, entry.0, entry.1).to_key_value(),
                None => unreachable!(),
            },
            _ => yaml_err!(self.name, "must contain exactly one \"key: value\" pair"),
        }
    }
    // ------------------------------------------------------------------------
    pub fn as_parser_pair(&'data self) -> Result<(Parser<'data>, Parser<'data>), String> {
        // special case (used rather often): a map with only one key:value pair
        match self.map.len() {
            1 => match self.map.iter().next() {
                Some(entry) => KeyValueParser::new(self.name, entry.0, entry.1).to_parser_pair(),
                None => unreachable!(),
            },
            _ => yaml_err!(self.name, "must contain exactly one \"key: value\" pair"),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'data> ListParser<'data> {
    // ------------------------------------------------------------------------
    #[allow(clippy::new_ret_no_self)]
    pub fn new(name: &'data str, data: &'data Yaml) -> Result<ListParser<'data>, String> {
        match *data {
            Yaml::Array(_, ref data) => Ok(ListParser { name, list: data }),
            _ => yaml_err!(format!(
                "expected type of {} to be list. found: {}",
                name,
                yaml_type(data)
            )),
        }
    }
    // ------------------------------------------------------------------------
    pub fn id(&self) -> &str {
        self.name
    }
    // ------------------------------------------------------------------------
    pub fn iter(&'data self) -> Box<dyn Iterator<Item = Parser<'data>> + 'data> {
        Box::new(
            self.list
                .iter()
                .enumerate()
                .map(move |(i, value)| Parser::new(format!("{} #{}", self.name, i + 1), value)),
        )
    }
    // ------------------------------------------------------------------------
    pub fn as_parser_list(&'data self) -> Vec<Parser<'data>> {
        self.iter().collect()
    }
    // ------------------------------------------------------------------------
    pub fn as_list_of_n_parser(&'data self, count: usize) -> Result<Vec<Parser<'data>>, String> {
        let result = self.as_parser_list();

        if result.len() == count {
            Ok(result)
        } else {
            yaml_err!(
                self.name,
                format!(
                    "expected list of {} elements. found: {}",
                    count,
                    result.len()
                )
            )
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
fn yaml_type(yaml: &Yaml) -> &str {
    match *yaml {
        Yaml::Real(_) => "float",
        Yaml::Integer(_) => "integer",
        Yaml::String(_) => "string",
        Yaml::Boolean(_) => "boolean",
        Yaml::Array(_, _) => "list",
        Yaml::Hash(_) => "key value table",
        Yaml::Alias(_) => "alias",
        Yaml::Null => "null",
        Yaml::BadValue => "bad value",
        Yaml::Comment(_, _) => "comment",
        Yaml::NewLine(_) => "new line",
    }
}
// ----------------------------------------------------------------------------
