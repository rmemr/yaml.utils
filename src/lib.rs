//
// yaml parser/writer
//
extern crate indexmap;
extern crate linked_hash_map;
extern crate yaml_rust;

#[macro_use]
extern crate log;

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub use yaml_rust::yaml::{Array, Hash};
pub use yaml_rust::Yaml;

pub mod encoder {
    pub use yaml_encoder::*;
    pub use yaml_rust::YamlEmitter;
}
pub mod parser {
    pub use yaml_parser::*;
    pub use yaml_rust::YamlLoader;
}

// ----------------------------------------------------------------------------
pub trait YamlLoader {
    // ------------------------------------------------------------------------
    fn read_yaml(filename: &str, verbose: bool) -> Result<Yaml, String> {
        // TODO Bufreader with chars iterator would be nice but api is unstable
        let mut buffer = String::new();

        match File::open(filename) {
            Ok(mut file) => {
                debug!("found {}", filename);
                match file.read_to_string(&mut buffer) {
                    Ok(size) => trace!("read {} bytes", size),
                    Err(why) => return Err(format!("read error: {}", why)),
                }
            }
            Err(why) => return Err(format!("error opening file: {}", why)),
        };

        if verbose {
            info!("parsing yaml file: {}", filename);
        } else {
            debug!("parsing yaml file: {}", filename);
        }
        let mut docs = match parser::YamlLoader::load_from_str(&buffer) {
            Ok(data) => data,
            Err(err) => return Err(format!("yaml parser: {}", err)),
        };
        trace!("file is valid yaml");

        // No need for multi document support. In fact something went wrong if
        // there are multiple docs
        if docs.len() != 1 {
            return Err(format!(
                "expected 1 YAML doc but found {} in file",
                docs.len()
            ));
        }

        // doc is a yaml::Yaml
        match docs.pop() {
            Some(yaml) => Ok(yaml),
            None => Err(String::from("empty yaml file?")),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
pub trait YamlWriter {
    // ------------------------------------------------------------------------
    fn write_yaml(outputfile: &Path, header: &str, data: &Yaml) -> Result<(), String> {
        use encoder::YamlEmitter;

        // FIXME target should be a TextWriter which implements Write trait
        let mut data_str = String::new();
        {
            let mut emitter = YamlEmitter::new(&mut data_str);
            emitter
                .dump(data)
                .map_err(|e| format!("yaml writer: {:?}", e))?;
        }
        // workaround for specifying comments in yaml
        // let mut data_str = data_str.replace("_YAML_COMMENT_:", "#");

        if !data_str.is_empty() {
            data_str.push('\n');

            debug!("> creating {}...", outputfile.display());

            let mut buf = File::create(outputfile).map(BufWriter::new).map_err(|e| {
                format!(
                    "yaml writer: couldn't create {}: {}",
                    outputfile.display(),
                    e
                )
            })?;

            buf.write(header.as_bytes())
                .map_err(|e| format!("yaml writer: write error: {}", e))?;
            buf.write(data_str.as_bytes())
                .map_err(|e| format!("yaml writer: write error: {}", e))?;
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
mod yaml_encoder;
#[macro_use]
mod yaml_parser;
// ----------------------------------------------------------------------------
use std::fs::File;
use std::io::{BufWriter, Read, Write};
use std::path::Path;
// ----------------------------------------------------------------------------
